<?php
/*  Controller
*   The controller object is the glue between
*   a View(template to render), and models(data).
*   The controller does the logic behind rendering
*   a view by working with models, then rendering
*   the view.
*/

namespace CosmicFramework\MVC;

use CosmicFramework\MVC\View;

class Controller {
    // The view object the controller will use to render, MUST be a CosmicFramework\MVC view object!
    public $view;

    public function __construct($view_name, $view_path) {
        $this->view = new View($view_name, $view_path);
        $this->view->setItems([]);
    }
    
    // Renders the view object
    public function render() {
        $this->view->render();
    }
}